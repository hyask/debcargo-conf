#!/usr/bin/make -f

%:
	dh $@ --buildsystem cargo

# regenerating the source code
# the xmlstarlet fixes are taken from upstream here: https://github.com/gtk-rs/gir-files/blob/master/fix.sh
execute_before_dh_auto_build:
	cp /usr/share/gir-1.0/GLib-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GObject-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Gst-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GModule-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GstBase-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GstAudio-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GstVideo-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GstGL-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GstGLEGL-1.0.gir $(CURDIR)
	xmlstarlet ed --inplace \
	--delete '//_:record[@name="GLMemoryEGL"]' \
	--delete '//_:record[@name="GLMemoryEGLAllocator"]' \
	--delete '//_:record[@name="GLMemoryEGLAllocatorClass"]' \
	--delete '//_:record[@name="EGLImage"]' \
	--delete '//_:record[@name="GLDisplayEGLDeviceClass"]' \
	--delete '//_:class[@name="GLMemoryEGLAllocator"]' \
	--delete '//_:class[@name="GLDisplayEGLDevice"]' \
	--delete '//_:callback[@name="EGLImageDestroyNotify"]' \
	--delete '//_:constant[@name="GL_MEMORY_EGL_ALLOCATOR_NAME"]' \
	--delete '//_:function[starts-with(@name, "egl")]' \
	--delete '//_:function[starts-with(@name, "gl_memory_egl")]' \
	--delete '//_:function[@name="is_gl_memory_egl"]' \
	--delete '//_:function-macro[starts-with(@name, "egl")]' \
	--delete '//_:function-macro[starts-with(@name, "EGL")]' \
	--delete '//_:function-macro[starts-with(@name, "GL_MEMORY_EGL")]' \
	--delete '//_:function-macro[starts-with(@name, "IS_EGL_IMAGE")]' \
	--delete '//_:function-macro[starts-with(@name, "IS_GL_MEMORY_EGL")]' \
	--delete '//_:method[@c:identifier="gst_gl_display_egl_from_gl_display"]' \
	--delete '//_:method[@c:identifier="egl_from_gl_display"]' \
	GstGLEGL-1.0.gir
	xmlstarlet ed -L \
	-u '//*[@glib:error-domain="g-option-context-error-quark"]/@glib:error-domain' -v g-option-error-quark \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_boolean_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gboolean*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_double_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gdouble*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_integer_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gint*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_locale_string_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "const gchar* const*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_string_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "const gchar* const*" \
	GLib-2.0.gir
	xmlstarlet ed -L \
	-u '//_:class[@name="Object"]/_:method[@name="getv"]//_:parameter[@name="names"]/_:array/@c:type' -v "const gchar**" \
	-u '//_:class[@name="Object"]/_:method[@name="getv"]//_:parameter[@name="values"]/_:array/@c:type' -v "GValue*" \
	-u '//_:class[@name="Object"]/_:method[@name="setv"]//_:parameter[@name="names"]/_:array/@c:type' -v "const gchar**" \
	-u '//_:class[@name="Object"]/_:method[@name="setv"]//_:parameter[@name="values"]/_:array/@c:type' -v "const GValue*" \
	-u '//_:class[@name="Object"]/_:constructor[@name="new_with_properties"]//_:parameter[@name="names"]/_:array/@c:type' -v "const char**" \
	-u '//_:class[@name="Object"]/_:constructor[@name="new_with_properties"]//_:parameter[@name="values"]/_:array/@c:type' -v "const GValue*" \
	-i '//_:interface[@name="TypePlugin" and not(@glib:type-struct)]' -t 'attr' -n 'glib:type-struct' -v 'TypePluginClass' \
	-i '//_:record[@name="TypePluginClass" and not(@glib:is-gtype-struct-for)]' -t 'attr' -n 'glib:is-gtype-struct-for' -v 'TypePlugin' \
	GObject-2.0.gir
	xmlstarlet ed --inplace \
	--delete '//_:record[@name="VideoAncillary"]/_:field[@name="data"]/_:array/@length' \
	--delete '//_:record[@name="VideoAncillary"]/_:field[@name="data"]/_:array/@fixed-size' \
	--insert '//_:record[@name="VideoAncillary"]/_:field[@name="data"]/_:array' \
	--type attr --name 'fixed-size' --value '256' \
	--delete "//_:member[@c:identifier=\"GST_VIDEO_BUFFER_FLAG_ONEFIELD\"][2]" \
	--delete "//_:member[@c:identifier=\"GST_VIDEO_FRAME_FLAG_ONEFIELD\"][2]" \
	--delete "//_:member[@c:identifier=\"GST_NAVIGATION_MODIFIER_META_MASK\"][2]" \
	GstVideo-1.0.gir
	sed -i 's/girs_directories\s=\s\[\"\.\.\/\.\.\/\.\.\/gir-files\",\s\"\.\.\/\.\.\/\.\.\/gst-gir-files\"\]/girs_directories=\[\".\"\]/' $(CURDIR)/Gir.toml
	gir -o .

# Remove the .gir file before install
execute_before_dh_auto_install:
	rm $(CURDIR)/*.gir
